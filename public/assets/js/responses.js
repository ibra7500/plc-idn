function getBotResponse(input) {

    //convert string to lowercase
    var inputlow = input.toLowerCase();
    //rock paper scissors
    if (inputlow == "apa itu plc?") {

        return `Prima Lexika Cendekia (PLC) merupakan badan usaha yang bergerak di bidang kebijakan publik, penelitian dan pendidikan hukum. 
        Untuk lebih lengkapnya anda bisa klik <a href = #about>Tentang Kami</a>`;
    }

     else if (inputlow == "apa saja kegiatan plc?") {
        return `Kegiatan PLC bisa anda lihat di <a href = #portfolio>Kegiatan Kami</a>`;
    }

    else if (inputlow == "kontak") {
        return `Kontak bisa anda lihat di <a href = #contact>Kontak</a>`;
    }  
    // Simple responses
    if (inputlow == "halo") {
        return "Halo!";
    } else if (input == "selamat tinggal") {
        return "Sampai jumpa!";
    } else {
        return `Maaf saya tidak bisa menjawab pertanyaan anda, silahkan hubungi kami lebih lanjut melalui <a href = #contact>Kontak</a> kami`;
    }
}