<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index()
    {
        return view('home/index');
    }

    public function publikasi()
    {
        return view('home/publikasi');
    }

    public function detail_buku()
    {
        return view('home/detail_buku');
    }
}
