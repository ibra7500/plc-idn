@extends('layouts/global')
@section('content')

<!-- ======= Hero Section ======= -->
<section id="hero">
  <div id="heroCarousel">

    <div class="carousel-inner" role="listbox">

      <div class="carousel-item active" style="background-image: url(assets/img/bg.jpg)">
        <div class="carousel-container">
          <div class="container">
            <p class="animate__animated animate__fadeInUp">Prima Lexika Cendekia</p>
            <h2 class="animate__animated animate__fadeInDown">Mengembangkan dan</h2>
            <h2 class="animate__animated animate__fadeInDown">Mengenalkan Dunia</h2>
            <h2 class="animate__animated animate__fadeInDown">Hukum kepada Publik.</h2>
            <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Ketahui Lebih
              Lanjut</a>
          </div>
        </div>
      </div>

    </div>
</section>
<!-- End Hero -->

<main id="main">

  <!-- ======= About Section ======= -->
  <section id="about" class="about">
    <div class="container" data-aos="fade-up">

      <div class="section-title">

        <p align="center">Apa yang Kami Lakukan?</p>
      </div>

      <div class="row content">
        <div class="col-lg-4">
          <h2>
            <b>Kebijakan Publik</b>
          </h2>
          <p>

          </p>
          <p align="justify">
            Kajian atas kebijakan publik secara akademis dan kritis untuk memberikan analisis dan pemahaman yang
            menyeluruh terhadap hal yang lahir oleh proses hukum berkenaan dengan masyarakat
          </p>
        </div>

        <div class="col-lg-4">
          <h2>
            <b>Penelitian Hukum</b>
          </h2>
          <p>

          </p>
          <p align="justify">
            Penelitian ilmiah yang didasarkan pada suatu metode, sitematika dan pemikiran dengan cara mengumpulkan dan
            menganalisa data untuk memberikan konsep pemecahan atas permasalahan.
          </p>
        </div>

        <div class="col-lg-4">
          <h2>
            <b>Pendidikan Hukum</b>
          </h2>
          <p>

          </p>
          <p align="justify">
            Proses dan penciptaan suasana pembelajaran untuk memberikan pemahaman dan pencerdasan kepada lapisan
            masyarakat dan pengembangan potensi diri.
          </p>
        </div>

      </div>

    </div>
  </section>
  <!-- End About Section -->

  <!-- ======= Cta Section ======= -->
  <section id="cta" class="cta">
    <div class="container" data-aos="zoom-in">

      <div class="text-center">
        <a class="cta-btn">Prima Lexika Cendekia</a>
        <p>

        </p>
        <p>

        </p>
        <h3>Trusted Partner for</h3>
        <h3>Knowledge and Solution</h3>


      </div>

    </div>
  </section>
  <!-- End Cta Section -->

  <!-- ======= Team Section ======= -->
  <section id="team" class="team section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">

        <p align="center">Profil Kami</p>
      </div>



      <div class="row">
        <div class="col-lg-4 mb-5" data-wow-delay="0.1s">
          <div class="member" data-aos="zoom-in" data-aos-delay="100">
            <img src="assets/img/team/fajri.jpg" class="img-fluid" alt="">
          </div>
          <div class="card shadow p-3 mb-5 bg-white rounded">
            <header class="card__title">
              <h4 align="center">Mohamad Fajri Mekka Putra</h4>
            </header>

            <main class="card__description">
              <p align="justify">
                Pendiri Prima Lexika Cendekia, Konsultan Good Corporate Governance, Peneliti Senior Dan Pengajar Program
                Studi Magister Kenotariatan Fakultas Hukum Universitas Indonesia (FHUI)
              </p>

              <h3>Pendidikan</h3>
              <ul>
                <li>Sarjana Hukum (SH), Universitas Indonesia, 2003</li>
                <li>Magister Kenotariatan (MKn.) Universitas Indonesia, 2006</li>
                <li>Mahasiswa Program Doktoral Ilmu Hukum, Universitas Brawijaya, 2020</li>
                <li>Certified Sustainability Reporting Specialist (CSRS), GRI-NCSR, 2015</li>
                <li>Certified Legal Auditor (CLA), BNSP, 2016</li>
              </ul>

              <h3>Bidang Keahlian</h3>

              <p>
                Good Corporate Governance, Hukum Perusahaan, Hukum Perjanjian, Penulisan Annual Report, Auditor/Assesor
                Hukum dan Kepatuhan ,Corporate Social Responsibility dan Laporan Berkelanjutan (Sustainability Report),
                Akta Notaris dan Hukum Pertanahan, Telah Menjadi Narasumber Sesuai Keahlian Bidangnya, Penulis Dan
                Sering Membantu Pembuat Kebijakan Dalam Mengkaji Kebijakan Bidang/Sektor
              </p>


            </main>
          </div>
        </div>

        <div class="col-lg-4 mb-5" data-wow-delay="0.2s">
          <div class="member" data-aos="zoom-in" data-aos-delay="200">
            <img src="assets/img/team/doddy.jpg" class="img-fluid" alt="">
          </div>

          <div class="card shadow p-3 mb-5 bg-white rounded">
            <header class="card__title">
              <h4 align="center">Doddy Riano Saputro</h4>
            </header>
            <main class="card__description">
              <p align="justify">
                Direktur Prima Lexika Cendekia, Managing Partner Pada EQUITY Law Office, Praktisi HR Dan Legal Counsel
                Pada Perusahaan Asing Dan Pengajar Luar Biasa Mata Kuliah Manajemen Hubungan Industrial Pada Beberapa
                Perguruan Tinggi
              </p>

              <h3>Pendidikan</h3>
              <ul>
                <li>Sarjana Hukum (SH), Universitas Indonesia, 2005</li>
                <li>Master Hukum (MH), Universitas Indonesia, 2017</li>
                <li>Advokat Peradi, 2011</li>
                <li>Certified Human Capital Management (CHCM), BNSP, 2018</li>
              </ul>

              <h3>Bidang Keahlian</h3>
              Hukum Perusahaan, Hukum Kekayaan Intelektual, Merger & Akusisi, Pidana Korporasi, Hukum Ketenagakerjaan,
              Manajemen Hubungan Industrial, Penyelesaian Perselisihan Hubungan Industrial, Kebijakan Industri,
              Community Development, Jaminan Sosial Nasional, Telah Terlibat pada Berbagai Organisasi Perusahaan APINDO,
              American Chamber, GAIKINDO, Indonesia Iron & Steel Industry Association (IISIA), Indonesian Labor Law
              Consultant Association (ILLCA) Dan Forum Studi Hukum Ketenagakerjaan FHUI.
            </main>
          </div>
        </div>


        <div class="col-lg-4 mb-5" data-wow-delay="0.3s">
          <div class="member" data-aos="zoom-in" data-aos-delay="300">
            <img src="assets/img/team/ririen.jpg" class="img-fluid" alt="">
          </div>
          <div class="card shadow p-3 mb-5 bg-white rounded">
            <header class="card__title">
              <h4 align="center">Ririen Aryani</h4>
            </header>

            <main class="card__description">
              <p align="justify">
                Spesialis Peneliti dan Manager Kemitraan, Advokat Pada EQUITY Law Office, Mediator Independen Dan
                Praktisi Legal Counsel Pada Perbankan Syariah
              </p>

              <h3>Pendidikan</h3>
              <ul>
                <li>Sarjana Hukum (SH), Universitas Indonesia, 2004</li>
                <li>Master Hukum (MH), Universitas Indonesia, 2019</li>
                <li>Advokat Peradi, 2012</li>
                <li>Mediator, Pusat Mediator Nasional, 2018</li>
              </ul>
              <h3>Bidang Keahlian</h3>
              <p>
                Hukum Perusahaan, Hukum Perjanjian, Hukum Perbankan Syariah, Ekonomi Syariah, Mediator Independen Di
                Pengadilan Agama.
              </p>
            </main>
          </div>
        </div>

      </div>

    </div>
  </section><!-- End Team Section -->

  <!-- ======= Portfolio Section ======= -->


  <!-- ======= Why Us Section ======= -->
  <section id="portfolio" class="why-us section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <p align="center">Kegiatan Kami</p>
      </div>

      <div class="row">

        <div class="col-lg-4 mb-5">
          <div class="col-md-25 video-box align-items-stretch"
            style='background-image: url("assets/img/thumb/pic1.jpg");' data-aos="zoom-in" data-aos-delay="100">
            <a href="https://youtu.be/Bfuzc9D4Dlk" class="venobox play-btn mb-4" data-vbtype="video"
              data-autoplay="true"></a>
          </div>
        </div>

        <div class="col-lg-4 mb-5">
          <div class="col-md-25 video-box align-items-stretch"
            style='background-image: url("assets/img/thumb/pic2.jpg");' data-aos="zoom-in" data-aos-delay="100">
            <a href="https://youtu.be/_9EHCn7u04g" class="venobox play-btn mb-4" data-vbtype="video"
              data-autoplay="true"></a>
          </div>
        </div>

        <div class="col-lg-4 mb-5">
          <div class="col-md-25 video-box align-items-stretch"
            style='background-image: url("assets/img/thumb/pic3.jpg");' data-aos="zoom-in" data-aos-delay="100">
            <a href="https://youtu.be/bv7jHa1Um2M" class="venobox play-btn mb-4" data-vbtype="video"
              data-autoplay="true"></a>
          </div>
        </div>

        <div class="col-lg-4 mb-5">
          <div class="col-md-20 video-box align-items-stretch"
            style='background-image: url("assets/img/thumb/pic4.jpg");' data-aos="zoom-in" data-aos-delay="200">
            <a href="https://youtu.be/DHR7PSWiGTA" class="venobox play-btn mb-4" data-vbtype="video"
              data-autoplay="true"></a>
          </div>
        </div>

        <div class="col-lg-4 mb-5">
          <div class="col-md-20 video-box align-items-stretch"
            style='background-image: url("assets/img/thumb/pic5.jpg");' data-aos="zoom-in" data-aos-delay="200">
            <a href="https://youtu.be/En25kgRKFvQ" class="venobox play-btn mb-4" data-vbtype="video"
              data-autoplay="true"></a>
          </div>
        </div>

        <div class="col-lg-4 mb-5">
          <div class="col-md-20 video-box align-items-stretch"
            style='background-image: url("assets/img/thumb/pic6.jpg");' data-aos="zoom-in" data-aos-delay="200">
            <a href="https://youtu.be/kV2xW_s8dOc" class="venobox play-btn mb-4" data-vbtype="video"
              data-autoplay="true"></a>
          </div>
        </div>

        <div class="col-lg-4 mb-5">
          <div class="col-md-20 video-box align-items-stretch"
            style='background-image: url("assets/img/thumb/pic7.jpg");' data-aos="zoom-in" data-aos-delay="300">
            <a href="https://youtu.be/fl8DADE0ORQ" class="venobox play-btn mb-4" data-vbtype="video"
              data-autoplay="true"></a>
          </div>
        </div>

        <div class="col-lg-4 mb-5">
          <div class="col-md-20 video-box align-items-stretch"
            style='background-image: url("assets/img/thumb/pic8.jpg");' data-aos="zoom-in" data-aos-delay="300">
            <a href="https://youtu.be/WeiNfOjaz3U" class="venobox play-btn mb-4" data-vbtype="video"
              data-autoplay="true"></a>
          </div>
        </div>

        <div class="col-lg-4 mb-5">
          <div class="col-md-20 video-box align-items-stretch"
            style='background-image: url("assets/img/thumb/pic9.jpg");' data-aos="zoom-in" data-aos-delay="300">
            <a href="https://youtu.be/SQFy0W3vejc" class="venobox play-btn mb-4" data-vbtype="video"
              data-autoplay="true"></a>
          </div>
        </div>

      </div>

    </div>
  </section>
  <!-- End Why Us Section -->
  {{-- Publikasi Section --}}
  <section id="publikasi" class="why-us section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <p align="center">Publikasi Kami</p>
      </div>

      <div class="row">

        <div class="col-lg-4 mb-5">
          <div class="col-md-25 video-box align-items-stretch"
            style='background-image: url("assets/img/cover_buku/PT_Perorangan_untuk_Usaha_Mikro_Kecil.png");'
            data-aos="zoom-in" data-aos-delay="100">

          </div>
          <div class="card shadow p-3 mb-5 bg-white rounded">
            <header class="card__title">
              <h4 align="center">PT PERORANGAN UNTUK USAHA MIKRO KECIL (UMK)</h4>
            </header>

            <main class="card__description">
              <ul>
                <li>Penulis: Muhammad Faiz Aziz & Muhammad Arif Hidayah</li>
                <li>Penerbit: Prima Lexika Cendekia</li>
                <li>Halaman: 92 Halaman</li>
                <li>Ukuran: 15.5 cm x 24 cm</li>
                <li>Harga: Rp. 150,000,-</li>
                <li>ISBN: (Proses)</li>
              </ul>
              {{-- Anchor tag Detail Buku --}}
              <a href="{{route('publikasi.detail_buku')}}" target="_blank" class="btn btn-primary btn-block">
                Detail Buku
              </a>
              {{-- <p align="justify">
                Pendiri Prima Lexika Cendekia, Konsultan Good Corporate Governance, Peneliti Senior Dan Pengajar Program
                Studi Magister Kenotariatan Fakultas Hukum Universitas Indonesia (FHUI)
              </p> --}}
            </main>
          </div>
        </div>

  </section>

  <!-- ======= Contact Section ======= -->
  <section id="contact" class="contact section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <p align="center">Kontak Kami</p>
      </div>

      <div class="row">

        <div class="col-lg-6">

          <div class="row">
            <div class="col-md-12">
              <div class="info-box">
                <i class="bx bx-map"></i>
                <h3>Alamat Kami</h3>
                <p>Jl. Wahid Hasyim No.10D Jakarta Pusat, <br>
                  Jakarta - Indonesia, 10340</p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="info-box mt-4">
                <i class="bx bx-envelope"></i>
                <h3>Email Kami</h3>
                <p>contact@plc-idn.com</p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="info-box mt-4">
                <i class="bx bx-phone-call"></i>
                <h3>Telepon</h3>
                <p>+62 21-5085-8676</p>

              </div>
            </div>
          </div>

        </div>

        <div class="col-lg-6">
          <form action="mailto:contact@plc-idn.com" method="post" class="email-form" enctype="text/plain">
            <div class="form-row">
              <div class="col form-group">
                <input type="text" name="name" class="form-control" id="name" placeholder="Nama Lengkap"
                  data-rule="minlen:4" data-msg="Mohon masukkan nama Anda" />
                <div class="validate"></div>
              </div>
              <div class="col form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Email Anda"
                  data-rule="email" data-msg="Mohon masukkan e-mail anda" />
                <div class="validate"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" id="subject" placeholder="Judul"
                data-rule="minlen:4" data-msg="Mohon masukkan judul minimal 8 huruf" />
              <div class="validate"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required"
                data-msg="Mohon diisi pesannya" placeholder="Isi Pesan"></textarea>
              <div class="validate"></div>
            </div>
            <div class="text-center"><button type="submit">Kirim Pesan</button></div>
          </form>
        </div>

      </div>

    </div>
  </section>
  <!-- End Contact Section -->

  <!-- Modal Sinopsis Buku -->
  <div class="modal fade" id="sinopsis1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Sinopsis PT PERORANGAN UNTUK USAHA MIKRO KECIL (UMK)</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p class="text-justify">
            Bentuk usaha Perseroan Terbatas (PT) selama ini telah lama dikenal sebagai format pendirian yang identik dan
            dipersepsikan sebagai usaha yang lebih besar.
            Pendekatan tersebut tentunya tidak lepas dari kenyataan preferensi pelaku Usaha Mikro dan Kecil (UMK) yang
            lebih memilih menggunakan bentuk Commanditaire
            Vennotschap (CV) dan perusahaan perorangan atau sole proprietorship. Undang-Undang No. 11 Tahun 2020 tentang
            Cipta Kerja yang kemudian diganti dengan Peraturan
            Pemerintah Pengganti Undang-Undang (Perppu) No. 2 Tahun 2022 memperkenalkan konsep Perseroan Perorangan atau
            yang mudah dikenal dengan PT Perorangan.
          </p>
          <p class="text-justify">
            Hadirnya bentuk hukum usaha ini menjadi alternatif bagi usaha mikro kecil untuk memulai usahanya dengan
            legalitas yang mudah dan terjangkau.
            Kehadiran bentuk usaha ini diharapkan dapat menyerap generasi wirausaha muda Indonesia dalam menyongsong
            bonus demografi Indonesia dimana pemerintah hendak
            memanfaatkan hal tersebut untuk kemajuan ekonomi nasional.
          </p>
          <p class="text-justify">
            Buku ini membahas mekanisme pendirian, manfaat bentuk usaha PT Perorangan, hingga proses likuidasi dan
            pembubaran PT Perorangan.
          </p>
          <p class="text-justify">
            Buku ini ditulis dengan penuh perhatian atas kondisi dan kontribusi UMK-M yang selama ini menjadi tulang
            punggung perekonomian Indonesia.
          </p>
          <p class="text-justify">
            Buku ini ditujukan bagi siapapun yang hendak menambah pengetahuan dan memanfaatkan PT Perorangan untuk
            usahanya, terutama pelaku UMK.
          </p>
          <p class="text-justify">
            Usai membaca buku ini, pembaca diharapkan dapat turut serta dalam pengembangan ekonomi UMK di negeri ini.
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
        </div>
      </div>
    </div>
  </div>
</main>
<!-- End #main -->

@endsection
