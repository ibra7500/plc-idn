@extends('layouts/global')
@section('content')

<main id="main" class="mt-5">

  <!-- ======= Why Us Section ======= -->
  <section id="publikasi" class="why-us section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title mt-5 pt-5">
        <p align="center">Publikasi Kami</p>
      </div>

      <div class="row">

        <div class="col-lg-4 mb-5">
          <div class="col-md-25 video-box align-items-stretch"
            style='background-image: url("assets/img/cover_buku/PT_Perorangan_untuk_Usaha_Mikro_Kecil.png");'
            data-aos="zoom-in" data-aos-delay="100">

          </div>
          <div class="card shadow p-3 mb-5 bg-white rounded">
            <header class="card__title">
              <h4 align="center">PT PERORANGAN UNTUK USAHA MIKRO KECIL (UMK)</h4>
            </header>

            <main class="card__description">
              <ul>
                <li>Penulis: Muhammad Faiz Aziz & Muhammad Arif Hidayah</li>
                <li>Penerbit: Prima Lexika Cendekia</li>
                <li>Halaman: 92 Halaman</li>
                <li>Ukuran: 15.5 cm x 24 cm</li>
                <li>Harga: Rp. 150,000,-</li>
                <li>ISBN: (Proses)</li>
              </ul>
              {{-- Button Modal Sinopsis --}}
              <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#sinopsis1">
                Sinopsis
              </button>
              {{-- <p align="justify">
                Pendiri Prima Lexika Cendekia, Konsultan Good Corporate Governance, Peneliti Senior Dan Pengajar Program
                Studi Magister Kenotariatan Fakultas Hukum Universitas Indonesia (FHUI)
              </p> --}}
            </main>
          </div>
        </div>

  </section>
  <!-- End Why Us Section -->

  <!-- Modal -->
  <div class="modal fade" id="sinopsis1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Sinopsis PT PERORANGAN UNTUK USAHA MIKRO KECIL (UMK)</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p class="text-justify">
            Bentuk usaha Perseroan Terbatas (PT) selama ini telah lama dikenal sebagai format pendirian yang identik dan dipersepsikan sebagai usaha yang lebih besar. 
            Pendekatan tersebut tentunya tidak lepas dari kenyataan preferensi pelaku Usaha Mikro dan Kecil (UMK) yang lebih memilih menggunakan bentuk Commanditaire 
            Vennotschap (CV) dan perusahaan perorangan atau sole proprietorship. Undang-Undang No. 11 Tahun 2020 tentang Cipta Kerja yang kemudian diganti dengan Peraturan 
            Pemerintah Pengganti Undang-Undang (Perppu) No. 2 Tahun 2022  memperkenalkan konsep Perseroan Perorangan atau yang mudah dikenal dengan PT Perorangan. 
          </p>
          <p class="text-justify">
            Hadirnya bentuk hukum usaha ini menjadi alternatif bagi usaha mikro kecil untuk memulai usahanya dengan legalitas yang mudah dan terjangkau. 
            Kehadiran bentuk usaha ini diharapkan dapat menyerap generasi wirausaha muda Indonesia dalam menyongsong bonus demografi Indonesia dimana pemerintah hendak 
            memanfaatkan hal tersebut untuk kemajuan ekonomi nasional.
          </p>
          <p class="text-justify">
            Buku ini membahas  mekanisme pendirian, manfaat bentuk usaha PT Perorangan, hingga proses likuidasi dan pembubaran PT Perorangan.
          </p>
          <p class="text-justify">
            Buku ini ditulis dengan penuh perhatian atas kondisi dan kontribusi UMK-M yang selama ini menjadi tulang punggung perekonomian Indonesia.
          </p>
          <p class="text-justify">
            Buku ini ditujukan bagi siapapun yang hendak menambah pengetahuan dan memanfaatkan PT Perorangan untuk usahanya, terutama pelaku UMK.
          </p>
          <p class="text-justify">
            Usai membaca buku ini, pembaca diharapkan dapat turut serta dalam pengembangan ekonomi UMK di negeri ini.
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
        </div>
      </div>
    </div>
  </div>


</main>

@endsection