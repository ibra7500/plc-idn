@extends('layouts.global')
@section('content')
    <!-- ======= Team Section ======= -->
    <section id="" class="section-bg mt-5">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <p align="center">PT PERORANGAN UNTUK USAHA MIKRO KECIL (UMK)</p>
            </div>
            <div class="container">
                <div class="member text-center" style="height: 25%;">
                    <img src="{{ asset('assets/img/cover_buku/PT_Perorangan_untuk_Usaha_Mikro_Kecil.png') }}"
                        style="width: 40%;" alt="">
                </div>
                <div class="card shadow p-3 bg-white rounded">
                    <main class="card__description">
                        <h3>Detail Buku</h3>
                        <ul>
                            <li>Penulis: Muhammad Faiz Aziz & Muhammad Arif Hidayah</li>
                            <li>Penerbit: Prima Lexika Cendekia</li>
                            <li>Halaman: 92 Halaman</li>
                            <li>Ukuran: 15.5 cm x 24 cm</li>
                            <li>Harga: Rp. 150,000,-</li>
                            <li>ISBN: (Proses)</li>
                        </ul>

                        <h3>Sinopsis</h3>
                        <p class="text-justify">
                            Bentuk usaha Perseroan Terbatas (PT) selama ini telah lama dikenal sebagai format pendirian
                            yang identik dan
                            dipersepsikan sebagai usaha yang lebih besar.
                            Pendekatan tersebut tentunya tidak lepas dari kenyataan preferensi pelaku Usaha Mikro dan
                            Kecil (UMK) yang
                            lebih memilih menggunakan bentuk Commanditaire
                            Vennotschap (CV) dan perusahaan perorangan atau sole proprietorship. Undang-Undang No. 11
                            Tahun 2020 tentang
                            Cipta Kerja yang kemudian diganti dengan Peraturan
                            Pemerintah Pengganti Undang-Undang (Perppu) No. 2 Tahun 2022 memperkenalkan konsep Perseroan
                            Perorangan atau
                            yang mudah dikenal dengan PT Perorangan.
                        </p>
                        <p class="text-justify">
                            Hadirnya bentuk hukum usaha ini menjadi alternatif bagi usaha mikro kecil untuk memulai
                            usahanya dengan
                            legalitas yang mudah dan terjangkau.
                            Kehadiran bentuk usaha ini diharapkan dapat menyerap generasi wirausaha muda Indonesia dalam
                            menyongsong
                            bonus demografi Indonesia dimana pemerintah hendak
                            memanfaatkan hal tersebut untuk kemajuan ekonomi nasional.
                        </p>
                        <p class="text-justify">
                            Buku ini membahas mekanisme pendirian, manfaat bentuk usaha PT Perorangan, hingga proses
                            likuidasi dan
                            pembubaran PT Perorangan.
                        </p>
                        <p class="text-justify">
                            Buku ini ditulis dengan penuh perhatian atas kondisi dan kontribusi UMK-M yang selama ini
                            menjadi tulang
                            punggung perekonomian Indonesia.
                        </p>
                        <p class="text-justify">
                            Buku ini ditujukan bagi siapapun yang hendak menambah pengetahuan dan memanfaatkan PT
                            Perorangan untuk
                            usahanya, terutama pelaku UMK.
                        </p>
                        <p class="text-justify">
                            Usai membaca buku ini, pembaca diharapkan dapat turut serta dalam pengembangan ekonomi
                            UMK di negeri ini.
                        </p>
                    </main>
                </div>
            </div>
        </div>
    </section><!-- End Team Section -->
@endsection
